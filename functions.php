<?php

/**
 * Inlcude tsmith512's WordPress utility and cleaning functions
 */
include ('functions-tsmith.php');

/**
 * sarahpowell_enqueue_scripts() runs on wp_enqueue_scripts to add
 * theme-specific CSS and JS. Bail if we're in admin.
 */
function sarahpowell_enqueue_scripts() {
  if ( is_admin() ) return;

  // The primary stylesheet
  $sarahpowell_styles = (get_stylesheet_directory_uri() . '/css/main.css');
  wp_enqueue_style('sarahpowell_styles', $sarahpowell_styles);

  // The colorbox stylesheet
  $sarahpowell_cb_styles = (get_stylesheet_directory_uri() . '/css/colorbox.css');
  wp_enqueue_style('sarahpowell_cb_styles', $sarahpowell_cb_styles);

  // Modernizr: load in head to prevent the flash of unstyled content
  $sarahpowell_modernizr = (get_stylesheet_directory_uri() . '/js/modernizr-custom.min.js');
  wp_enqueue_script('sarahpowell_modernizr', $sarahpowell_modernizr, null, null, false );

  // Colorbox: for gallery images
  $sarahpowell_colorbox = (get_stylesheet_directory_uri() . '/js/jquery.colorbox-min.js');
  wp_enqueue_script('sarahpowell_colorbox', $sarahpowell_colorbox, array( 'jquery' ), null, true );

  // The theme-specific javascript
  $sarahpowell_scripts = (get_stylesheet_directory_uri() . '/js/main.js');
  wp_enqueue_script('sarahpowell_scripts', $sarahpowell_scripts, array( 'jquery', 'sarahpowell_colorbox' ), null, true );
}
add_action('wp_enqueue_scripts','sarahpowell_enqueue_scripts');
